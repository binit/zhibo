<?php
$serv = new swoole_websocket_server("0.0.0.0",3999);
//服务的基本设置
$serv->set(array(
	'worker_num' => 2,
	'reactor_num'=>8,
	'task_worker_num'=>1,
	'dispatch_mode' => 2,
	'debug_mode'=> 1,
	'daemonize' => true,
	'log_file' => __DIR__.'/log/webs_swoole.log',
	'heartbeat_check_interval' => 60,
	'heartbeat_idle_time' => 600,
));

$serv->on('connect', function ($serv,$fd){
	echo "client:$fd Connect.".PHP_EOL;
});

$serv->on("receive",function(swoole_server $serv,$fd,$from_id,$data){
	echo "receive#{$from_id}: receive $data ".PHP_EOL;
});

$serv->on('open', function($server, $req) {
	echo "server#{$server->worker_pid}: handshake success with fd#{$req->fd}".PHP_EOL;;
});

$serv->on('message',function($server,$frame) {
	$msg['data']=$frame->data;
	switch ($msg['type']){
		case 'login':
			$server->push($frame->fd,"欢迎欢迎~");
		break;
		default:
		break;
	}
	$msg['fd']=$frame->fd;
	$server->task($msg);
	//$server->task($frame->data);
});

$serv->on("workerstart",function($server,$workerid){
	echo "workerstart: ".$workerid.PHP_EOL;
});

$serv->on("task","on_task");

$serv->on("finish",function($serv,$task_id,$data){
	echo "finish";
	return;
});
$serv->on('close', function($server,$fd,$from_id) {
	echo "connection close: ".$fd;
});

$serv->start();
function on_task($serv,$task_id,$from_id,$msg) {
	$data = $msg['data'];
	$data_arr = explode('|', $data);
	$str_arr = explode('=', $data_arr[0]);
	$type = $str_arr[0];
	if(!empty($type)){
		$chat_id = $data_arr[1];
		$fd = $msg['fd'];
		$key = "client".$fd;
		if($chat_id != false && $chat_id != 'false'){
			$_SESSION["$key"] = $chat_id;
		}
		$chat_id = $_SESSION["$key"];
		$umsg_arr = array();
		if($type == "Login" || $type == "Ulogin"){
			$show_type = "Ulogin";
		}elseif($type == "SendMsg" || $type == "UMsg"){
			$show_type = "UMsg";
		}elseif($type == "UonlineUser"){
			$show_type = "roomUserlist";
		}elseif($type == "Ulogout"){
			$show_type = "Ulogout";
		}
		$msg_arr['stat'] = 'OK';
		$msg_arr['type'] = $show_type;
		if ($type == 'SendMsg') {
			$umsg_arr['ChatId'] = $chat_id;
			$umsg_arr['ToChatId'] = $str_arr[2];
			if ($str_arr[2] != 'ALL') {
				$umsg_arr['IsPersonal'] = "true";
			}else{
				$umsg_arr['IsPersonal'] = "false";
			}
			$umsg_arr['Style'] = $data_arr[2];
			$umsg_arr['Txt'] = $data_arr[3];
		}else{
			$umsg_arr['roomid'] = 1;
			$umsg_arr['chatid'] = $chat_id;
			$umsg_arr['nick'] = '游客'.$chat_id;
			$umsg_arr['sex'] = 0;
			$umsg_arr['age'] = 0;
			$umsg_arr['qx'] = 0;
			$umsg_arr['ip'] = '59.175.39.166';
			$umsg_arr['vip'] = '繁华落尽';
			$umsg_arr['color'] = 0;
			$umsg_arr['cam'] = 0;
			$umsg_arr['state'] = 0;
			$umsg_arr['mood'] = '';
		}
		$msg_arr["$show_type"] = $umsg_arr;
	}else{
		$msg_arr['type'] = 'ping';
	}
	foreach ($serv->connections as $conn) {
file_put_contents('/var/www/html/swoole/swoole-src-1.8.7-stable/log.txt',"$conn\n", FILE_APPEND);
		$serv->push($conn,json_encode($msg_arr));
	}
	return;
}
function on_finish($serv,$task_id,$data){
	return true;
}
